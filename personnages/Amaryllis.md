# Amaryllis

Enfant elfe de 15 ans.
Blonde, et menue, elle est souvent accompagnée d'un loup.

Dotée d'un passé qu'elle ignore, elle a vécu ses 14 premières années dans la forêt avec un père adoptif druide et elfe.

Habillée avec une peau de bête et des végétaux tressés, elle a quitté sa forêt pour découvrir le monde.
Un monde violent, qui fait peur.