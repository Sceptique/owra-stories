# Arc 2 - Les Ukwards se lèvent

Oberyn a accepté de suivre son destin.
Devenir roi des Ukwards, les nains.

Aujourd'hui, il lui fait encore négocier avec le Duc, prendre part à la guerre qui a commencé, et réunifier son peuple.

________________________

Dans les jours qui suivirent le sacre d'Oberyn, plusieurs réunions eurent lieux avec les deux chefs de clan présents :

- Gilmin Acier accueille la venue du nouveau roi avec bienveillance, il propose à Oberyn de diriger la ville des Acier et le soutien dans ses choix.

- Raffar Zeclat n'a pas daigné se déplacer pour le sacre de son nouveau roi... Oberyn a tenté un dernier geste amical dans sa direction en lui envoyant une lettre lui expliquant qui il était et quels étaient ses objectifs. Il le prie de bien vouloir entamer les négociations afin de connaitre, en retour, les désirs de Raffar pour l'avènement des Ukwards.

- Predor H&C, quant à lui, est un peu moins réceptif au couronnement d'Oberyn cependant, après une longue discussion, ils tombèrent d'accord sur un point important : On ne peut laissé Raffar Zeclat piétiner l'autorité du nouveau roi et, si il n'entend pas raison suite à la lettre d'Oberyn, ils sont prêt à attaquer la ville grâce à des cartes des chemins souterrains (carte qui lui a donné Predor).
 Oberyn pense qu'il faut être sûr d'avoir les effectifs militaires suffisants en cas de combat, il refléchit dont à un moyen de libérer Egild. Une idée lui vint mais pour ça il lui faudra rencontrer la comtesse de la ville... ainsi que réunir assez d'or.