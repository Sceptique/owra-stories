# Partie 1 : La tête de l'ogre

### Chapitre 1 : Aube

Discussion avec Fervil.
Rencontre avec 2 nouveaux compagnons.
Discussion entre eux et mauvaise ambiance.
Départ le matin à l'aube.

### Chapitre 2 : Tuer et brûler

Déplacement dans la forêt.
Arrivée au fort.
Entrée discrète.
Cadavres, vigile, cartes.
Arrivée dans le bâtiment principal.
Norbak trouve une trappe, où se trouve des gardes apeurés.

### Chapitre 3 : Regroupement

Discussion avec les gardes. Puis départ de ces derniers vers Öm.
Repérage sur la carte des positions ennemies et du fort 13.
Déplacement jusqu'au fort 13.
Ils arrivent ensuite dans un village pittoresque où des gobelins civils se font martyriser par des orcs.
Combat rapide contre les orcs.
Accueil en soirée par le sergent Karim qui leur explique la situation.

### Chapitre 4 : Le piège
Le sergent leur propose d'aller trouver les prisonniers.
Ils suivent les traces jusqu'à une grosse clairière avec le groupe d'orcs et un ogre, ainsi que les prisonniers. Ils préparent un plan pour les vaincre et se mettent en position.

### Chapitre 5 : L'assaut

Combat contre les monstres. Les prisonniers sont libérés.

(on passe sous silence leur retour au fort)