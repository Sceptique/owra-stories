Rufus s'arrêta d'un coup, et sentit un parfum dont la fragrance fit bondir son coeur. Son sang ne fit qu'un tour, il prévint succintement Oberyn qu'il devait partir en urgence, et s'enfuya suivant la piste de cette odeur familière.  
Peu après, il était sorti du village, et continua en direction de la forêt. Là, il tomba nez à nez avec une jeune femme.
Elle était vêtue d'une longue cape sombre, sous laquelle on pouvait voir miroiter de nombreux bijoux et autres breloques. Elle avait d'interminables cheveux couleur jais, et ses yeux perçants - l'un marron, et l'autre vert - ne laissaient à Rufus aucun doute quant à sa nature. Il ne put s'empêcher de l'aborder maladroitement:

 **Rufus**: Vous... êtes-vous... une changeline?  
 **Inconnue**: Une... Changeline ?  
 **Inconnue**: Pourquoi me poser cette question, monsieur..?  
 **Rufus**: Ah, pardonnez-moi, je dois paraître tout à fait louche.  
 **Rufus**: Je m'appelle Rufus Stradiel, ensorceleur de l'impossible !  
 **Rufus**: Puis-je vous demander ce que vous faites dans un tel endroit?  
 **Inconnue**: Faites-vous partie de cette compagnie qui a aidé les étrangers à envahir le village ?  
 **Rufus**: Hum, dit comme ça...  
 **Rufus**: Pour répondre à votre question honnêtement, je fais bien partie de cette compagnie, mais nous n'avons fait que sauver des gens d'une fin tragique.  
_L'attitude de la jeune femme change soudainement. Jusque là réservée, son visage se fend d'un sourire et elle fait un pas vers Rufus._  
 **Inconnue**: Je vous ai cherchés depuis ce jour !  
 **Rufus**: Moi?  
 **Inconnue**: Je vous suis redevable pour tout ce que vous avez pu faire pour moi.  
 **Rufus**: S'est-on déjà rencontrés?  
 **Inconnue**: Oh, je ne pense pas que vous m'ayez rencontrée en personne. Cependant, cela fait plusieurs jours maintenant que je vous observe.  
 **Inconnue**: Dans tous les cas, vous m'avez libérée de ce village, et vengé ma soeur, et pour cela je vous suis reconnaissante.  
 **Rufus**: Je suis perdu.  
 **Rufus**: Qui était votre soeur?  
 **Inconnue**: Oh, c'était la personne la plus douce du monde.  
 **Inconnue**: J'ai passé mes dix-sept premières années en sa compagnie ; c'était mon guide, mon idole, elle veillait sur moi et notre vie était idyllique.  
 **Inconnue**: Nous vivions en bordure de ce village, à l'orée de la forêt - hélas, ce qui était notre maison n'est aujourd'hui qu'un tas de cendres parmi les autres.  
 **Inconnue**: Il y a de cela trois mois, les villageois ont jeté leur dévolu sur ma chère soeur, l'accusant de sorcellerie.  
 **Inconnue**: Ils ont incendié notre maison, la forçant à sortir, puis l'ont emmenée de force et l'ont brûlée en place publique.  
 **Inconnue**: Ce soir là, j'étais allée me promener en forêt, et ce n'est qu'en revenant que je me suis apperçue du crime sans nom qui allait être fait.  
 **Inconnue**: Je n'ai eu le temps de rejoindre la place que pour voir ma propre soeur brûler devant mes yeux - et avec elle, toute mon enfance.  
 **Inconnue**: Depuis, j'erre dans la forêt, ne m'éloignant jamais du village, cherchant une occasion pour me venger de ces humains abjects qui m'ont dérobé tout ce que j'avais de plus cher.  
 **Inconnue**: Et enfin, vous êtes arrivés, et avez apporté à cet endroit maudit l'objet de sa destruction.
 **Rufus**: Je.. je ne sais quoi dire, vous avez vécu un tel drame...  
 **Inconnue**: Mais maintenant, vous etes là, et c'est fini.  
 **Rufus**: Les choses se sont-elles réellement déroulées ainsi? Les réfugiés ont semé le chaos dans le village?  
 **Inconnue**: Oui, eux aussi ont du faire face à la haine implacable des habitants.  
 **Inconnue**: Tous les jours, des enlèvements, des meurtres ! Et lorsque les réfugiés ont osé répliquer, voyez comme on leur a répondu - avec des pierres et des épées !  
 **Rufus**: Vous n'avez pas un indice sur ce qui a motivé ces crimes précédant la révolte?  
 **Inconnue**: Non, aucun, si ce n'est que ce n'est pas la première fois que les habitants de ce village s'en prennent aux plus faibles.  
 **Rufus**: Que voulez-vous dire?  
 **Rufus**: Il y a eu d'autres conflits avant tout ça?  
 **Inconnue**: Oh, ne faites pas revenir ces horribles souvenirs.  
 **Inconnue**: Ma soeur n'était pas la première à être tuée de la sorte.  
 **Inconnue**: Avant elle, il y eut mon père, mais j'étais trop jeune pour m'en souvenir ; et encore avant, qui sait combien d'autres ont perdu la vie ?  
 **Rufus**: Changeons de sujet, je vois que cela vous fait souffrir.  
 **Rufus**: Depuis combien de temps vivez-vous ainsi dans la forêt?  
 **Rufus**: Vous êtes seule?  
 **Inconnue**: Trois mois, je vous l'ai dit, depuis la mort de ma soeur.  
 **Inconnue**: Je n'ai plus personne désormais, et plus rien ne me retient sur ces lieux  
 **Rufus**: Puis-je vous demander pourquoi votre famille a subi de telles persécutions tout ce temps? Pourquoi ne vivez-vous pas au sein du village?  
 **Rufus**: Si vous ne souhaitez pas répondre, je comprendrai.  
 **Inconnue**: Je ne sais pas exactement pourquoi, nous avons toujours vécu à l'écart... Après le déchainement de haine que j'ai pu voir de la part des habitants, je comprends que ma famille a voulu nous mettre à l'abri, ma soeur et moi.  
 **Rufus**: Les évènements risquent de s'enchaîner assez vite dans les alentours, je ne pense pas que la révolte finisse dans de bonnes conditions.  
 **Rufus**: Vous n'êtes pas à l'abri du danger si vous restez dans les parages.  
 **Inconnue**: Eh bien... Vous etes des aventuriers, n'est-ce pas ?  
 **Inconnue**: Je suppose que vous ne comptez pas vous éterniser ici.  
 **Rufus**: Souhaitez-vous me suivre? Je dois retrouver mes compagnons, une fois réunis nous pourrons vous emmener dans une ville où vous ne craindrez rien.  
 **Inconnue**: Oui, je vous suivrai avec plaisir.  
 **Rufus**: Bien.  
 **Inconnue**: J'espère pouvoir me rendre utile, c'est le moins que je puisse faire pour vous remercier.  
 **Rufus**: Avant de repartir, j'aurais une dernière question.  
 **Inconnue**: Je suis toute ouïe.  
 **Rufus**: Quel est ce parfum que vous portez? Il me semble familier.  
 **Inconnue**: Oh, c'est ma soeur qui m'a appris à le faire... C'est un mélange de fleurs sauvages qu'on trouve dans cette forêt.  
 **Inconnue**: A propos, je me nomme Tharja.  
 **Tharja**: Maintenant, si vous n'avez plus de questions, peut-être devrions-nous rejoindre vos compagnons ? Ils pourraient avoir besoin de votre aide.  
 **Rufus**: Oui, aucun doute là-dessus, partons.  

Le soir tombait, nos deux compagnons décidèrent de s'abriter pour la nuit.  
Le lendemain, ils débutèrent leur long voyage pour Arkan. Sur leur chemin, loin au nord de Shön, ils pouvaient apercevoir avec horreur des cadavres empalés autour du village en cendres. Une forte présence militaire peuplait désormais la zone.

Nos deux compagnons rencontrèrent plus tard un marchant itinérant qui partait de Owyl vers Arkan. Ce dernier accepta volontiers de les emmener contre quelques contes et péripéties du charmant ensorceleur.  
Ils parvinrent ainsi à rallier Arkan au bout de deux jours.
