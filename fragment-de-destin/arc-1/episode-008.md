# Partie 8 : Les ténèbres

### Chapitre 1 : Les pièges
Les compagnons ont combattu plusieurs groupes d'araignées, et sont blessés.
Après quelques heures de repos dans le temple, ils redescendent combattre des Ettercaps.
Rufus est gravement empoisonné durant la bataille, et Norbak devient totalement introuvable.
Le groupe le cherche plusieurs minute mais décide de remonter dans le temple pour y soigner Rufus, qui semble très mal en point.

### Chapitre 2 : Tunnels sculptés
Après une soirée et des soins magiques intensifs, ils décident d'en terminer.
Ils arrivent dans des tunnels immenses et creusés par des créatures intelligentes.
Ils découvrent deux immenses portes en pierre et un long couloir.
Ouvrant la porte de droite, ils arrivent au niveau d'une salle immense avec un guerrier au milieu.

### Chapitre 3 : Les guerriers sombres
Le guerrier s'avance vers eux, impassible devant leurs premiers appels.
Ils détectent trois ennemis invisibles qui semblent les entourer.
Le premier guerrier est abattu par des sorts de Rufus et un coup de Oberyn, mais il abat une sphère de ténèbres autour de lui et du nain, ce qui le coupe à la vue des autres membres du groupe.
Le deuxième essaie de tuer Amaryllis et Rufus en profitant de son invisibilité, mais échoue dans sa manœuvre.
Le magicien invisible échoue à tuer qui que ce soit malgré plusieurs éclairs.
Les deux guerriers tués, le mage prend la fuite, alors qu'Amaryllis émet une lumière puissante qui chasse les ténèbres magiques.

### Chapitre 4 : Le temple des ténèbres
Ils avancent dans la salle et les couloirs, et arrivent au niveau d'un salle encore plus immense et richement décorée.
Ils y voient une énorme araignée qui les ignore.
Amaryllis part en éclaireur et voit une prêtresse drow.
Elle repart vers ses compagnons, suivie par la drow.

Cette dernière arrive vers eux et bloque la lumière de Amaryllis.
Celle-ci se nome "Irileth Do Dumund".
La prêtresse discute avec eux, et ils apprennent que les drows réclament la propriété des ruines naines. Les aventuriers partent, prétextant d'avoir besoin d'un peu de temps.
La prêtresse dit à Amaryllis qu'elle n'est pas "du même monde que ces autres compagnons", ce qui semble la déstabiliser, et Oberyn est sûr d'une seule chose: il ne laissera pas les drows gagner.

### Chapitre 5 : Les sols déchus
Le groupe retourne dans le temple nain. Là, les lumières qui étaient apparues disparaissent petit à petit.
Ils accourent vers la statue, et ils y constatent que celle-ci semble corrompue et couverte de veinules noires.
Enfin, ils se rendent compte que le cœur de la statue semble avoir été arraché.

Rufus trouvent une porte cachée au fond de la pièce.
Ils l'empruntent, et sortent des tunnels, loin au sud du village dans le bois calciné.