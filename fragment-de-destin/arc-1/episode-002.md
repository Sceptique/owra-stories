# Partie 2 : La tête du dragon

### Chapitre 1 : Rufus

Rencontre avec Rufus au fort.
Le sergent leur explique leur prochaine mission, qui consiste en la sécurisation d'un monastère, qui dispose d'un avantage stratégique pour la guerre qui s'annonce.

### Chapitre 2 : Tanuki

L'équipe débute un voyage à travers la forêt de pins.
Durant leur trajet ils sont surpris par un Tanuki qui les invite à boire le thé.

### Chapitre 3 : La tour de l'archer

Arrivé devant la tour.
Ils font le tour et arrivent devant la porte.
Discussion de comment entrer.
Rufus va sur le coté en attendant et le dragon sort.

### Chapitre 4 : Dragons !

Combat contre les 2 reptiles.
Entré dans la tour.
Combat contre 2 squelettes.
Esprit frappeur arrive.
Repartent voir le Tanuki pour la nuit.

### Chapitre 5 : Une rencontre inattendue

Soirée chez le Tanuki.
Le matin, attaque des aventuriers.
Insultes, mais diplomatie de justesse.
Retour à la tour.

### Chapitre 6 : L'âme perdue

Séparation en 2 groupes.
Rufus bard et léon contre le fantome.
Combat contre le poltergeist.

### Chapitre 7 : Les parents

Se retrouvent devant la porte principale.
Départ du 2sd groupe.
Les compagnons attaquent les 2 dragons en haut de la tour.
Ils trouvent un arc enchanté et le point de vue.
"Retournons au fort. Nous avons réussis."
