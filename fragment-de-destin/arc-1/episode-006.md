# Partie 6 : La roi sous la montagne

### Chapitre 1 : Exploration sous-terraine
Glissent dans l'escalier et arrivent en bas très vite. Il y a des maisons creusées dans le plateau rocheux qui se situe dans une immense caverne. Ils fouillent une maison ou deux mais ne trouvent rien que des indices sur les centaines d'années qui se sont écoulées.
Ils trouvent une taverne avec des fûts d'alcool nain ancestraux dont ils récupèrent quelques bouteilles.
Ils sortent et descendent un escalier fossilisé qui mène à un plateau en dessous sur lequel ils devinent la présence de quelques silhouettes.

### Chapitre 2 : Les habitants
Ils traversent un village inférieur plus riche et se rendent compte qu'ils sont suivis. Oberyn se planque dans un coin pendant que les autres servent d'apat et surprend les 3 troglodytes qui les suivent. Ils finissent pas se faire un peu comprendre et le groupe suit les reptiliens.
Le chaman de la tribu se fait comprendre via un draconien approximatif et les invite à un banquet sanglant autour d'une créature des profondeurs.

### Chapitre 3 : La faille
Discussion avec le Chaman qui est l'ennemi du RSLM. Ils préparent un plan pour le vaincre ensemble. Ils vont ensuite capturer les Duegars qu'ils trouvent en haut à l'aide des troglodytes (qui en mangent deux). Ils le font parler le dernier en le menacant un peu et découvrent qu'il en reste 2 (qu'ils tuent aussi).

### Chapitre 4 : La forge primaire
Ils préparent leur plan. Des troglodytes se préparent à faire péter la sortie avec des rochers, d'autres se préparent à utiliser le charbon des forges pour faire des projectiles enflammés à l'aides des scorpions nains.
Le RSLM arrive et ils le combattent. Oberyn resiste assez de temps pour leur permettre de le vaincre, il fuit après avoir soufflé une fois le froid. Ils n'arrivent pas à le bloquer quand il fuit et disparait.
Le chaman et Amaryllis accourent pour soigner Oberyn qui tient ses tripes avec ses mains. Il est très gravement blessé mais ils utilisent toute leur forces pour soigner le nain.

### Chapitree 5 : Transmutation
Découverte d'une pièce cachée, un genre de temps. Oberyn y prie le St Patron et se voit contrôlé par un être qui le designe à travers son corps comme son héritier.