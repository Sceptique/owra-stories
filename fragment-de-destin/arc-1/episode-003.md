# Partie 3 : Les pierres du destin

### Chapitre 1 : Le repos du guerrier

Arrivée au fort.
Karim les remercie.
Ils rentrent avec un nain.
À Öm, village militarisé.

### Chapitre 2 : L’honnêteté ne s'achète pas

Arrivé à Arkan.
Le nain va à Fortefer.
Quand il revient, il voit Norbak dans une ruelle.
Combat avec les bandits.
Arrestation par la garde

### Chapitre 3 : Cambrioleur

Amaryllis et Rufus s'inquiètent.
Le lendemain libération.
Le soir ils cambriollent.

### Chapitre 4 : Les pierres

Estimation des pierres.
La pierre magique.
Gardes de la baronne Elisabeth interviennent.

### Chapitre 5 : L'aventure reprend

Rencontre avec Elisabeth.
La pierre s'active.
