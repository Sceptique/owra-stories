# Partie 9 : Incendie de société

### Chapitre 1 : Un nouveau soleil
Une fois sortis du temple nain, le groupe se rend compte que le coeur de la statue est certainement l'artefact qu'ils recherchent.
Ils se mettent sur les traces du voleur, en direction du nord.
En arrivant au village de Shön, ils rencontrent un inquisiteur, Arnaud, envoyé par Salomon, le haut prêtre de Pélor.
Celui-ci se joint à eux car il recherche des informations pour son église.
Le groupe accepte ce compagnon temporaire pour la traque.

### Chapitre 2 : Fumées de détresse
Les aventuriers traversent la forêt, partiellement de nuit, et arrivent à proximité du village de Shön dans la soirée suivante.
Le village semble en feu, et des fumées inquiétantes s'en élèvent.
Le groupe arrive à proximité du village, et ils entendent clairement les clameurs de combat.
Ils découvrent alors une ville en proie au chaos et à la révolte. Il semble qu'il s'agisse des réfugiés amenés plusieurs jours plus tôt.

### Chapitre 3 : Émeutiers et chaos
L'inquisiteur fonce devant des émeutiers, qui se trouvent êtres les migrants du sud.
Il tente d’apaiser la foule, mais il est submergé, abandonné par les gardes qu'il avait rejoint.
Il combat un certain temps, tuant plusieurs émeutiers avant de prendre la fuite, grièvement blessé par un d'entre eux, armé d'une faux.

### Chapitre 4 : Trouver le voleur
Pendant ce temps, Amaryllis, Rufus, Oberyn et Kulay suivent un garde qui fuyait.
Ils y apprennent que les émeutiers sont les migrants du sud, qui semblent vouloir se venger de plusieurs assassinats.
Le groupe part en direction des prisons pour y retrouver Norbak, mais, sur place, ils ne trouvent qu'un vieillard rabougris à moitié mort.
Amaryllis s'est posée sous forme d'aigle, plus loin. Les deux nains et Rufus devant la prison, regardent avec amertume le chaos et la mort, et le village qui disparaît progressivement dans les flammes et la violence.
