# Corbeau noir

## Chapitre 1: Colonie

- arrivée des personnages à la colonie de Cheterfield
- informe les villageois que le village précédent à été pillé

## Chapitre 2: L'escorte

- les 3 cavaliers accompagnent le marchang Kihl à Cheterfield
- une loutre produit une fausse alerte
- un troll des marais attaquent en même temps mais le groupe l'élimine avec quelques blessures mineures

## Chapitre 3: Taverne et repos

- tout le monde à la taverne, festivement
- Faraday produit un blanc en entendant "sorcière"

## Chapitre 4: Querelle et chasse

- le lendemain elle entend un paysan parler de sabbat, sacrifice, sorcière
- suite à une querelle, et à un intérogatoire Faraday et Theodric se retrouve à vouloir chasser les sorcières
- Cordelia informe Maure la maire que le groupe veut attaquer
- Heron le bucheron propose d'accompagner le groupe

## Chapitre 5: Ombreuse

- Cordelia part dans l'après midi et accompagne un paysan Emile pour amener la chèvre et éviter au village d'être associé à ces aventuriers aggressifs
- les deux sont attaqués pendant la nuit par un ours qui dévore le cheval de Cordelia
- Cordelia&Emile arrivent à proximité d'Ombreuse, et Teo&Faraday&Heron arrivent peu après
- ils remarquent un enorme corbeau dans le ciel

## Chapitre 6: Premier sang

- ils attendent la nuit, après quoi le groupe est reveillé par des présence de sorcières
- ils se font attaquer après que Cordelia soit partie et après qu'elle leur ait dit ce qu'elle avait à dire
- le groupe tue les 3 sorcières et tentent de les intérroger grace aux pouvoirs d'inquisiteurs de Téo, mais ils en apprennent trop peu
- tout le monde se repli dans un camp de bucheron sécurisé pour finir la nuit.