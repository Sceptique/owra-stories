# La chasse aux sorcières

## Chapitre 1: Fouilles

- rentrent dans Ombreuse (la colline)
- fouillent partout
- esquivent des pièges
- tuent un serpent géant
- trouvent des trésors cachés
- renversent un chaudron, trouvent des masque, une pierre bizarre
- traces de serpent géant, sorcières corbeaux

## Chapitre 2: Le procès

- reviennent au village
- chevaliers du nord arrivent
- rapport à Maure
- procès (Heron viré du village, les autres sortis de la taverne)

## Chapitre 3: L'enquête

- incendie controlé de nuit
- traces de sang le matin
- trouvent le cadavre (Emile) et des indices (epine, fille étrange)

## Chapitre 4: La cavalerie

- les chevaliers partent au sud (le groupe les accompagne)
- visitent ombreuse et mènent leur enquête
- rentré au village après avoir trouvé des indices
- identification des objets magiques (elfique)
- donnent un indice à un chevalier-mage

## Chapitre 5: Second sang

- reveil en pleine nuit (les sorcières brulent le village)
- combat à l'ouest du village
- l'incendie est maitrisé
- 6 colons morts, 1 chevalier mort, 15 sorcières tuées (dont 7 par les aventuriers)