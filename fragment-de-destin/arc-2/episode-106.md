# Bataille décisive

## Chapitre 1: la forêt

Le groupe est envoyé en éclaireur. Ils préparent des pièges pour la bataille à venir.
Ils se rendent ensuite à l'est du village où ils ont repéré un chemin qui longe la rivière.
Ils le suivent jusqu'à une forêt claire dans laquelle ils rencontrent un ours blessé.
Faraday commence à l'attaqué, protégé par Cordélia, mais le combat tourne vite mal. Théodric frappe du plat de sa lame pour éloigner la bête sans la blesser plus. La bête s'éloigne et laisse le groupe.
Ils se rendent également compte qu'un petit lutin les observe. Ils parviennent à discuter avec lui et il les emmène dans une clairière secrète.

Là "le grand chêne", un arbre parlant discute avec eux et leur dit qu'il les aidera contre les sorcières.

## Chapitre 2: le plan

Les aventuriers préparent un plan de bataille.
18 chevalier, la capitaine, leur magicien, 3 scorpions.
Ils découvrent également les ennemis et leurs forces.

## Chapitre 3: la bataille

Les chevaliers repoussent la première vague de sorcière, puis résistent tant bien que mal à la charge de monstre bestiaux énormes. Enfin, les 3 morigannes sont abattues.
Les survivantes en fuite sont pourchassées et plusieurs sont faites prisonniers, interrogées, puis abattues.

Le groupe sait qu'il reste une trentaine de sorcières (ils en ont tué au moins une trentaine plus les monstres), la chef des morigannes et une "grande prêtresse du père".

Le groupe se prépare pour l'assaut final.
