# Rituel

## Chapitre 1 : 3 silhouettes
Les aventuriers se rendent dans la caverne après que 3 nouveaux morts soient découverts.
La chevalier percoit un element étrange dans un couloir en partant, mais l'ignore.
Après avoir traversé la caverne principale, ils vont à droite et y trouve une pièce peu éclairée naturellement, conçue par une race intelligente, et avec une rampe menant vers une porte.
Derrière, il y trouvent un rituel executé par 3 moines rouge blanc et noir.
Ils essaient d'intervenir mais sont repoussés par une barrière magique.
Ils sont attaqués brievement par quelques oiseaux étranges.
Ils s'éloignent ensuite rapidement en fermant la porte, voyant que le rituel monte en intensité.

## Chapitre 2 : La faille
Ils vont dans la caverne de gauche.
Là, ils découvrent une faille dans le plafond haut de 50 mètres, et essaient d'y accéder.
Ils trouvent en chemin une forme de foetus d'oiseau dans des champignons.
Malgré leur entetement, ils ne parviennent pas à passer l'imposant devers de la caverne, et Faraday se blesse grievement en chutant.
Elle est soigné sommairement par Tharja, et ils retournent ensuite au village.
En repassant par la caverne principale, ils découvrent une nuée d'oiseaux semblablent au précédents, et le lac souterrain qui s'était changé en sang.
Arrivés, ils découvrent de nouveaux morts.