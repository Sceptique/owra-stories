# La fin des sorcières

## Chapitre 1: Le duel

Après avoir pansé leurs blessures, le groupe et les chevaliers partent vers le repère des sorcières que les éclaireurs avaient vu quelques jours plus tôt. Cependant, ils se rendent compte que la route a changé depuis leur dernier passage : devant eux se tient un immense gouffre qui n'était pas là avant. Le seul moyen de le traverser est un pont étroit et abîmé, qui ne tiendra probablement pas le poids des chevaliers...  

Cordélia commence à traverser le pont avec une corde, pensant le renforcer pour permettre le passage aux chevaliers. Mais alors qu'elle avance, le groupe aperçoit du mouvement de l'autre côté du pont : plusieurs sorcières - 6 ou 7 peaux d'écorce et une morrigan - leur font face.  

La morrigan s'approche du pont et demande aux aventuriers un duel loyal. Si les aventuriers gagnent le duel, les sorcières partiront; sinon, eux partiront et les sorcières resteront. La morrigan semble être sincère, et Faraday s'avance, acceptant le duel : c'est par elle que tout a commencé, et c'est par elle que tout doit finir.  

Le duel commence, et après plusieurs minutes d'action intense, la morrigan succombe aux coups déchaînés de Faraday. Victorieuse, celle-ci s'avance et rejoint l'autre côté du pont, où les sorcières se prosternent devant elle. Elle leur ordonne de quitter ces terres après leur avoir posé plusieurs questions sur son passé. D'après les sorcières, la sorcière responsable pour le bannissement de son esprit serait la grande Jiilsiid : la plus puissante des morrigan, qui depuis les a trahit et a renié leur dieu.  

Cette grande Jiilsiid serait sur le point de réapparaître. Les chevaliers, ayant le pressentiment qu'elle utilise le sabbat pour se rematérialiser, décident de se rendre sur le lieu du sabbat des sorcières pour être certains que toute menace est éliminée.

## Chapitre 2: Confrontation au sommet du sabbat

Après plusieurs heures de marche pénible dans les broussailles et la forêt, les aventuriers arrivent en vue du village des sorcières. Celui-ci est dévasté, et plusieurs cadavres de sorcières gisent à terre. Ils en concluent qu'il y a eu un affrontement entre les sorcières.  

Les aventuriers continuent à avancer jusqu'au sommet de la montagne où se trouvent le sabbat. Alors qu'ils s'en approchent, ils entendent des clameurs d'incantations. Une fois en haut, ils aperçoivent un groupe de sorcières en train de prier autour d'un autel. Au sommet de cet autel couvert de marches se trouve une statue immense représentant une sorcière. Devant cet autel se dresse un énorme chaudron en pierre bouillonnant.  

Après s'être concertés, les chevaliers chargent les sorcières et les mettent en déroute, en tuant plusieurs au passage. Cependant, le chaudron continue de bouillir de plus en plus fort, et la statue au sommet de l'autel semble leur lancer un regard moqueur...  

Le groupe a à peine le temps de souffler qu'une figure immense sort du chaudron. Ressemblant à une morrigan, la créature se jette sur eux et un affrontement sans merci commence. Après avoir essuyé quelques coups, la sorcière s'envole, empêchant les chevaliers de l'atteindre, et continue à les harceler avec ses sorts et son souffle putride.  

Heureusement, grâce à leurs arcs et arbalètes et aux scorpions qu'ils ont eu la présence d'esprit d'emmener, les héros viennent à bout de la grande Jiilsiid. Alors que son corps fond, une pierre semblable à celles déjà récupérées sur les morrigan en sort. Les aventuriers la récupèrent et la gardent de côté avec les autres.  

La statue qui avait regardé le combat s'anime, et révèle être la grande prêtresse des sorcières. Elle commence à leur fournir quelques vagues réponses à propos du culte des sorcières et du sabbat, mais les provocations répétées de Cordélia la lassent et elle finit par s'envoler et partir.

## Chapitre 3: Retour à Öm

Les chevaliers rentrent victorieux au village. Ils n'y restent pas longtemps et retournent à Öm, où se trouve leur quartier général. Les aventuriers demandent à les accompagner, n'ayant plus rien à faire ici. Sur le chemin, Ophélia examine les pierres et révèle à la compagnie leur utilité. Les pierres rouges servent à manipuler le feu, tandis que la pierre jaune et verte permet de voir tout ce qui est. L'utilité des pierres bleues, cependant, lui échappe.  

_18 juillet 2016_

Une fois à Öm, la compagnie se sépare. Théodric se rend à Arkhan pour récupérer sa récompense (les chevaliers ayant écrit des lettres de recommendation à chacun des aventuriers) et faire son rapport à son ordre. Cordélia, quant à elle, va faire son rapport à sa guilde. Alors qu'elle entre dans le bureau de son référent, Rufus, elle rencontre Samael, un homme à l'aspect pour le moins étrange, caché derrière une armure blanche.  

## Chapitre 5: Nouveau départ

Rufus demande à Cordélia de lui prêter les pierres afin qu'il puisse les étudier, ce qu'elle fait à contre-coeur. Il lui confie également sa mission suivante : il faut qu'elle aille enquêter sur un cambriolage qui a eu lieu dans la chapelle du manoir de son père. Accompagnée de Samael, elle se met en route.  

Les deux aventuriers font une escale à Öm, Cordélia étant désireuse de vendre certains objets qu'elle a accumulé pendant son voyage, ainsi que donner sa lettre des chevaliers à la guilde des aventuriers du duc. Là-bas, elle croise Théodric de nouveau, qu'elle convainc de l'accompagner.